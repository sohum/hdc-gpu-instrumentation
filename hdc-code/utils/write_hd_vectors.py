#!/usr/bin/python
# write_hd_vectors - writes HD vector array to file as  0s and 1s from an int array of -1 and +1
# mode <= 0
#   The hd vectors cam be rows of +1/-1 in an np array. In that case, the mode should be <= 0
#   The file has only have 0s and 1s, no comma(elemnt separator) or comments
# mode > 0
#   The HD vector can have any *integer* elements
#   They will be printed with a space separator

import numpy as np

def write_hd_vectors(fname, hd_array, mode):
    file = open(fname, 'w')

    if (mode <= 0):
        hd_array = 1*(hd_array > 0); # convert to 0/1
        ## Write the Item Memory Used
        for i in range(hd_array.shape[0]):
            arr = map(str, hd_array[i]);
            str_out = (''.join(arr))
            file.write(str_out + '\n');
        print("\nwrite_hd_vectors: Written binary vectors to %s" %(fname))
    else:
        for i in range(hd_array.shape[0]):
            arr = map(str, hd_array[i]);
            str_out = (' '.join(arr))
            file.write(str_out + '\n');
        print("\nwrite_hd_vectors: Written integer HD vectors to %s" %(fname))
    file.close()
