#
# create_item_memory_np.py - item memory initializer
# Sohum Datta
# November 2017, UC Berkeley
 

import tensorflow as tf
import numpy as np
import random as rnd
import math as mth
import os

# The Item Memory generated for hardware must be same across applications.

# This functions intitializes the constant item_memory. Bipolar Code.
#
# There are 2^10 = 1024 possible characters in Item Memory.
# The lower half of the character set (chars 1 - 1024) are constants.
# The upper half of the character set (chars 1025 onwards) are contiuous HD
# vectors satisfying triangle inequality (for Hamming distance)
# 
# The variable CIM_HEAD is the origin of continuous item memory. Its assigned
# an address 1 - 1024

# Assertion checks for create_item_memory:
# * D is tf.int32 with value > 256 and divisible by 256
# * CIM_HEAD is tf.int32 with value between 0 and 127 (included)

# Numpy version. Speeds up the computation if you don't have to reset the CIM
# head in the middle (mostly true). Bipolar Code.
# Also the number of (non-CIM) items can be changed 
# Checked manually -- done!



def create_item_memory_np(CIM_HEAD, D=2048, NUM_ITEMS=1023):
    # CIM_HEAD Is not implemented

    #if number of items required is larger than 1024, 
    # will randomly permute the basic 1024 vectors.
    # number of permutations needed (atleast once) is 
    #in the variable "num_reps".
    num_reps = (NUM_ITEMS/1024)+1
   
    #final_filename = ITEM_MEMORY_FILENAME + str(num_reps) + ".dat";
    
    #if(os.path.isfile(final_filename)):
    #    # The file exists
    #    file = open(final_filename, 'r');
    #    mem = np.fromfile(final_filename, dtype=np.int32);
    #   print "create_item_memory_np: Loaded Item Memory data "+ final_filename
    #    file.close();
    #else:
    # Make the item memory and save in file
    
    # Create the basic item memory first
    base = np.ones(D)
    pre_mem = list()
    for i in range(int(D/2)):
        base[i] = -1
    
    # create the basic 1024 items
    for j in range(1024):
        pre_mem.append(np.random.permutation(base))
    
    #These vectors below will be there in hardware ROM
    mem_hw = np.array(pre_mem)
    mem = mem_hw
    # The number of 1s and -1 are exactly equal 
    
    while (num_reps > 1):
        temp = np.transpose(mem_hw)
        #the random.permutation function below can only permute rows
        #we need column permutation. Hence transpose.
        new_mem = np.transpose(np.random.permutation(temp))
        mem = np.concatenate((mem, new_mem), axis=0)
        num_reps = num_reps - 1
    
    mem.astype(np.int32)
    
    return mem
    
