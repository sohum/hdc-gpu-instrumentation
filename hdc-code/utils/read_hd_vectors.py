#!/usr/bin/python
# read_hd_vectors - reads HD vector array written in a file as  0s and 1s to an int array of -1 and +1
# The file must only have 0s and 1s, no comma(elemnt separator) or comments
# mode <= 0
#   Reads a file of 0/1 without a separator, and converts it to -1/+1
# Mode > 0
#   Reads a file of integer HD elements separated by space

import numpy as np

def read_hd_vectors(fname, mode):
    array = list();
    for line in open(fname, 'r'):
        line = line.strip();    #remove newline
        if (mode <= 0):
            arr = np.array(map(int, list(line))); # make a row
            arr = 2 * arr - 1;
        else:
            arr = np.array(map(int, line.split())); # make a row
        array.append(arr);
    final_array = np.array(array);
    final_array.astype(np.int32)
    return final_array
