# make_dir.py -- checks for presence of a directory, creates if not present.
import os
import errno

def make_dir(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        # Raise exception if and only if that it already exists
        if exception.errno != errno.EEXIST:
            raise
