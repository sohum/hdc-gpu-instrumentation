from __future__ import print_function
from tensorflow.examples.tutorials.mnist import input_data
from tensorflow.contrib.data import Dataset, Iterator
import tensorflow as tf
import subprocess
import sys
import numpy as np

#Print full array
#np.set_printoptions(threshold='nan')


OUTPUT_DIR = "../mnist.build/"


mnist = input_data.read_data_sets(OUTPUT_DIR + "dataset/", one_hot=False)
## The dataset is in the above struct is saved in the folder above.
# mnist.train.images is a 55000x784 tensor with labels in mnist.train.labels
# mnist.validation.images is 5000x784 with labels in mnist.validation.labels
# mnist.validation.test is 10000x784 with labels in mnist.test.labels

train_dataset = Dataset.from_tensor_slices((mnist.train.images,
    mnist.train.labels))
train_iterator = Iterator.from_structure(train_dataset.output_types,
    train_dataset.output_shapes)
    
data_temp, class_num_temp = train_iterator.get_next()
data = tf.to_int32(tf.greater(data_temp, tf.constant(0.5, dtype=tf.float32)))
class_num = tf.cast(class_num_temp, tf.int32)

glob_init = tf.global_variables_initializer()
local_init = tf.local_variables_initializer()
table_init = tf.tables_initializer()
data_init = train_iterator.make_initializer(train_dataset)

print("Dumping MNIST Training dataset")

sess = tf.Session()

sess.run(glob_init)
sess.run(local_init)
sess.run(table_init)
sess.run(data_init)
    
# start the filename queue runner thread
coord = tf.train.Coordinator()
threads = tf.train.start_queue_runners(sess=sess, coord=coord) 
    
try:
    while not coord.should_stop():
        data_out, label_out = sess.run([data, class_num])
        data_disp = np.reshape(data_out, (28, -1))
        print("Label " + str(label_out) + ":=")    
        for row in range(28):
            for col in range(28):
                print("{:1.0f}".format(data_disp[row][col]), end=" ")
            print()
except Exception as e:
    coord.request_stop(e)
finally:
    coord.request_stop()
coord.join(threads)
sess.close()   
