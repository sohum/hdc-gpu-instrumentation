# -*- coding: utf-8 -*-
# printProgressBar.py - print a console progress bar with prefix and suffix
# Sohum Datta
# March 2018

# Sample Output
# <prefix>: |█████████████████████████████████████████████-----| 90.0% <suffix>
# * iteration - current itenration num, int
# * total - total number of iterations, int
# * prefix - prefix string as above, default null
# * suffix - suffix string as above, default null
# * decimals - number of decimeals in percentage complete, default 1
# * length - character length of bar, default 50. Must be integer
# * fill - bar fill character. Cannot be '-'
import sys

def printProgressBar(iteration, total, prefix='', suffix='', decimals=1,length=100, fill='█'):
    percent = ("{0:." + str(decimals) + "f}").format(100*(iteration/float(total)))
    filledLength = int((length*iteration)//total)
    bar = fill * filledLength + '-' * (length - filledLength)
    sys.stderr.write('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix))
    sys.stderr.flush()
#    if iteration == total: 
#        print "\n" #newline
    return
