from printProgressBar import *
from time import sleep

items = list(range(57))
l = len(items)
printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
for i, item in enumerate(items):
    # Do stuff...
    sleep(0.1)
    # Update Progress Bar
    if i % 5 == 0:
        print("\nThe item is " + str(i) + "\n")
    printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
