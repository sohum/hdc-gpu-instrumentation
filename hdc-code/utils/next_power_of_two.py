# next_power_of_two - returns an int which is the next power of 2 of x
def next_power_of_two(n):
    x = int(n)
    if x == 0:
        return 1
    if x & (x-1) == 0:
        return x
    while x & (x - 01) > 0:
        x = x & (x-1)
    return x << 1
