# print_dataset_stats - prints the statistics for the trained dataset
#
# Printed Info:
# Dimensionality - Input DIM
# Dataset Name - input DATASET
# Number of Classes - input NUM_CLASSES
# Number of Features - input NUM_FEATURES
# Number of Training examples - input NUM_TRAIN_EXAMPLES
# Number of Test examples - input NUM_TEST_EXAMPLES
# Final Norm of class vectoros in case of scaled cosine - inpur NORM_SCALE
# 
# For scaled cosine associative memory - inputs are:
#   . assoc_mem_out (the accumulated sums of the training graph, before
#   normalization)
#   . associative_memory_val_out (the scaled associative memory from Tensorflow testing graph)
#   Original Scaling constant - input DENOMINATOR
#   Scaling factor  - next power of two of DENOMINATOR
#   For each class:
#       minimum absolute value of HD vector element
#       maximum absolute value of HD vector element
#       mean absolute value of HD vector element
#       std. dev of absolute value of HD vector element

import numpy as np
import math
from next_power_of_two import *

def print_dataset_stats(DIM, DATASET, NUM_CLASSES, NUM_FEATURES,
        NUM_TRAIN_EXAMPLES, NUM_TEST_EXAMPLES, DENOMINATOR, 
        NORM_SCALE, assoc_mem_out, associative_memory_val_out):
    
    print("\n**********************************************************************")
    print(" DIM:\t\t " + str(DIM))
    print(" DATA INFO:\t " + DATASET)
    print(" NUM_CLASSES:\t " + str(NUM_CLASSES))
    print(" NUM_FEATURES:\t " + str(NUM_FEATURES))
    print(" TRAIN SIZE:\t " + str(NUM_TRAIN_EXAMPLES))
    print(" TEST SIZE:\t " + str(NUM_TEST_EXAMPLES))
    print("**********************************************************************\n")
    print("SCALING CONSTANT = " + str(DENOMINATOR) + ", SCALE = "
            +str(next_power_of_two(DENOMINATOR)) + ", FINAL NORM = " +
            str(NORM_SCALE))
    print("\nAccumulator bits >=" + 
        str(math.ceil(math.log(np.max(np.abs(assoc_mem_out)),2))))
    print("Squarer bits >=" + 
        str(math.ceil(math.log(np.max(np.abs(assoc_mem_out)),2)) -
            math.log(next_power_of_two(DENOMINATOR),2 )))
    print("\nFor absolute values of elements stored for each class i.e. normnalized and scaled associative memory:\n")
    for i in range(NUM_CLASSES):
        min_val = min(np.abs(associative_memory_val_out[i]))
        max_val = max(np.abs(associative_memory_val_out[i]))
        avg_val = np.mean(np.abs(associative_memory_val_out[i]))
        std_val = np.std(np.abs(associative_memory_val_out[i]))
        print("Class " + str(i)+": min="
            +str(min_val)+" max="+str(max_val)+
            " avg="+("{0:.2f}").format(avg_val) + " std="+ ("{0:.2f}").format(std_val))

    print("TOTAL: min="+str(np.min(np.abs(associative_memory_val_out)))+
        " max="+str(np.max(np.abs(associative_memory_val_out)))+
        " avg="+("{0:.2f}").format(np.mean(np.abs(associative_memory_val_out)))+
        " std="+("{0:.2f}").format(np.std(np.abs(associative_memory_val_out))))
