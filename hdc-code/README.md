*USAGE of CODE*
-----------------
* This folder contains all code and dataset requried for runnign GPU
  instrumentation tasks.
  
* To run any of the tree programs lang_rec, emg or MNSIT, simply do:
	python <exec_name> <item_memory_file> <associative_memory_file>

* The Item Memory for EMG and MNIST are common, given as "teim_memory.dat".
  The trained Associative Memory using the above Item Memory are respectively
  in "EMG_binaryAM.dat" and "MNIST_binaryAM.dat" for EMG and MNIST.

* For language Recognition, the item memory is given by "lang_item_memory.dat"
	and corresponding trained AM is in "lang_binaryAM.dat"

