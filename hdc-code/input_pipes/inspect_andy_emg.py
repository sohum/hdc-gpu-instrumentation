#!/usr/bin/python
import tensorflow as tf
import time
import os
import glob

from andy_emg_pipe import andy_emg_pipe

# This file is used to inspec the values stored in Andy's dataset1
# Manually inspection required. Tested OK

DIR='/home/sohumdatta/workdir/bindump';

#files = tf.train.match_filenames_once(DIR + "s1-sess1-set1-1.float32bin", name="files")
files = [DIR + '/s1-sess1-set1-1.float32hd']


key, label, channel_vals = andy_emg_pipe(files)
    
## get initializer ops
glob_init = tf.global_variables_initializer()
local_init = tf.local_variables_initializer()
table_init = tf.tables_initializer()
    
with tf.Session() as sess:
    # Initialize the tables and all variables
    sess.run(glob_init)
    sess.run(local_init)
    sess.run(table_init)

    # start the filename queue runner thread
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord) 
       
    try:
        while not coord.should_stop():
            key_out, label_out, channel_out = sess.run([key, label, channel_vals])
            print "*** key: " + str(key_out) + " | label=" + str(label_out) + ", channel_vals= " + \
                str(channel_out)
    except Exception as e:
        coord.request_stop(e)
    finally:
        coord.request_stop()
    coord.join(threads)
    sess.close()
