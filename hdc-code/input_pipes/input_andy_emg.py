import tensorflow as tf
import os

# Generates a Queue and Spatially encoded samples from 
# [label, encoded_spatial_HD_vector]
# from a list of binary stream filenames. 

def input_andy_emg(files, item_mem):
    # Create a Queue to hold filenames
    # files is a 1D tensor of filename strings
    item_type = tf.float32
    element_bytes = item_type.size

    # each sample has a label and 64 channels
    sample_bytes = 65 * element_bytes
    
    # max 60 files
    filename_queue = tf.train.string_input_producer(files,
            num_epochs=1, capacity=60)

    # Fixed Length Record Reader 
    reader = tf.FixedLengthRecordReader(record_bytes=sample_bytes)
    
    # Key below is ?
    # sample holds a string of bytes
    key, sample = reader.read(filename_queue)
    
    # converts the sample to a vector of float32 BigEndian
    sample_elements = tf.decode_raw(sample, tf.float32, little_endian=False)


    # NOTE: Left is an absolute path. Extract the filename with extensions
    # filename = tf.py_func(os.path.basename, [pathname], tf.string)

    # remove extension. This is assumed to be the label
    label_temp = sample_elements[0]
    label = tf.cast(label_temp, tf.int32)

    channel_vals = tf.cast(sample_elements[1:], tf.int32)

    # TODO: remove probe
    ######################################################################
    # Setup of SPATIAL ENCODER
    #----------------------------------------------------------------------
    # Spatial Encoding for FlexEMG:
    #  Spatial vector = Sum[[ channel_id (INTEGER-MULTIPLY) channel_value ]]
    
    # channel ID vectors, 64 channels total
    channel_vector = item_mem[0:64,:]
    
    # the quantized version of channel samples
    # TODO: setup the right scaling
    product_vector = tf.matmul(
        tf.diag(tf.to_int32(channel_vals)), channel_vector)  
    # threshold the product vector
    thresholded_vector = 2 * tf.to_int32(
        tf.greater(tf.reduce_sum(product_vector, 0), 0)) - 1

    return label, channel_vals, thresholded_vector
