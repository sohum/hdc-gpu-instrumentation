#ChoirDataIterator.py - returns a dataset from tensor slices and an iterator
#from a choir dataset. Supply only filename of choir data
# Returns:
# * number of classes
# * number of features in each vector
# * number of examples
# * dataset - a TensoforFlow dataset
# * iterator to the dataset above

#from tensorflow.contrib.data import Dataset, Iterator
from tensorflow.data import Dataset, Iterator
import numpy as np
import tensorflow as tf
import struct

#This functions opens the data files and upacks them
def ChoirDataIterator(filename):
    f = open(filename)
    nFeatures = struct.unpack('i', f.read(4))[0]
    nClasses = struct.unpack('i', f.read(4))[0]
    X = list()
    y = list()
    while True:
        newDP = list()
        for i in range(nFeatures):
	    v_in_bytes = f.read(4)
	    if v_in_bytes is None or len(v_in_bytes) == 0:
	        break
	    v = struct.unpack('f', v_in_bytes)[0]
	    newDP.append(v)
        else:
        # Only executed if for (bove) did NOT break
	    l = struct.unpack('i', f.read(4))[0]
	    X.append(newDP)
	    y.append(l)
            continue
        break #continue abouve ensures this executes when for DID break

    num_data = len(y)

    #Make Numpy arrays    
    X_arr = np.array(X)
    Y_arr = np.array(y)
    
    ##NOTE: Quantization of space done here
    mid_point = (np.amin(X_arr) + np.amax(X_arr))/2
    X_arr = 1*(X_arr > mid_point)
    
    X_arr.astype(np.float32)
    Y_arr.astype(np.int32)

    X_tensor = tf.constant(X_arr, dtype=tf.float32)
    Y_tensor = tf.constant(Y_arr, dtype=tf.int32)
    
    dataset = Dataset.from_tensor_slices((X_tensor, Y_tensor))
    iterator = Iterator.from_structure(dataset.output_types,
            dataset.output_shapes)
   
    #NOTE: Need to have atleast nFeatures + num_quantization items in Item
    #Memory
    return nClasses, nFeatures+2, num_data, dataset, iterator
