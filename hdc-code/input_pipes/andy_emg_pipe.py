import tensorflow as tf
import os

# Emits samples from binary files
# [label, raw_sample _array]
# from a list of filenames. 
# The format of data are as assumed to be encoded in the dataset.
# Each file *must* contain a sample which is to be trained on.

def andy_emg_pipe(files):
    # Create a Queue to hold filenames
    # files is a 1D tensor of filename strings
    item_type = tf.float32
    element_bytes = item_type.size

    # each sample has a label and 96 channels
    sample_bytes = 97 * element_bytes

    # max 60 files
    filename_queue = tf.train.string_input_producer(files,
            num_epochs=1, capacity=60)

    # Fixed Length Record Reader 
    reader = tf.FixedLengthRecordReader(record_bytes=sample_bytes)
    
    # Key below is ?
    # sample holds a string of bytes
    key, sample = reader.read(filename_queue)
    
    # converts the sample to a vector of float32 BigEndian
    sample_elements = tf.decode_raw(sample, tf.float32, little_endian=False)


    # Left is an absolute path. Extract the filename with extensions
    # filename = tf.py_func(os.path.basename, [pathname], tf.string)

    # remove extension. This is assumed to be the label
    label_temp = sample_elements[0]
    label = tf.cast(label_temp, tf.int32)

    channel_vals = sample_elements[1:]
    return key, label, channel_vals
