import tensorflow as tf
import os

# Generates a Queue and returns [label, text] from a list of filenames
# The filenames are assumed to be <label>.<extension>
# Each file *must* contain a line which is to be trained on.

def input_textline_pipe(files):
    # Create a Queue to hold filenames
    # files is a 1D tensor of filename strings
    filename_queue = tf.train.string_input_producer(files,
            num_epochs=1, capacity=50)

    # Text Line Reader 
    reader = tf.TextLineReader()
    
    # Key below is <filename>:<sentence_no>. need to parse it
    # text holds the sentence.  
    key, text = reader.read(filename_queue)

    # remove the ":number"
    pathname = tf.string_split([key], delimiter=':').values[0]

    # Left is an absolute path. Extract the filename with extensions
    filename = tf.py_func(os.path.basename, [pathname], tf.string)

    # remove extension. This is assumed to be the label
    label = tf.string_split([filename], delimiter='.').values[0]

    return label, text
