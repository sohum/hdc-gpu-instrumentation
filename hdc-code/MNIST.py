#!/usr/bin/env python
from tensorflow.examples.tutorials.mnist import input_data
#from tensorflow.contrib.data import Dataset, Iterator
from tensorflow.data import Dataset, Iterator
import tensorflow as tf
import time
import os
import glob
import math
import subprocess
import sys
import numpy as np

import argparse

from hdc_base.hlu import *
from hdc_base.assoc_mem import *
from hdc_base.encode_ngram import *

from input_pipes.ChoirDataIterator import *

from utils.make_dir import *
from utils.printProgressBar import *
from utils.next_power_of_two import *
from utils.print_dataset_stats import *
from utils.read_hd_vectors import *
from utils.write_hd_vectors import *
from utils.create_item_memory_np import *

#Print full array
#np.set_printoptions(threshold='nan')

## Argument setup
parser = argparse.ArgumentParser(description='HD Demo 1 simulator for MNIST Feature Superposition')
parser.add_argument("itemmemory", help="Relative path to the binary Item Memory for MNIST")
parser.add_argument("assocmem", help="Relative path to binary AM for MNIST")

args = parser.parse_args()

#Only ERROR messages by Tensorflow
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}

##################################################################################
#                       * MACHINE SIMULATION SETUP *
##################################################################################
### Please make changes below only!                                             

DATASET = "MNIST"

#Address of CIM Root (< 128)
CIM_HEAD = 127

##################################################################################
#                        * DIRECTORY SETUP *
##################################################################################

# HD Dimension (**DO NOT CHANGE **)
DIM = 2048


#CHOIR Dataset files are by Mohsen.
get_basename = lambda i: os.path.basename(i)

#CHOIR_TEST_DATA_FILE = ("/home/sohumdatta/workdir/dataset/feature_superposition/"
#            + DATASET + "/"+DATASET+"_test.choir_dat")
CHOIR_TEST_DATA_FILE = ("./dataset/"+DATASET+"_test.choir_dat")

##################################################################################
#                 * PROGRAM INTERNAL SETUP (NO CHANGES HERE) *
##################################################################################

ITEM_MEMORY = args.itemmemory
ASSOC_MEMORY = args.assocmem


##################################################################################
##################################################################################
#----------------------------------------------------------------------
#                   Testing Module
#----------------------------------------------------------------------

test_graph = tf.Graph()
with test_graph.as_default():
    #---------------------------------------------------------------------------------
    #                    Preparation of Testing data iterator
    #---------------------------------------------------------------------------------
    
    #NOTE: Use line below for Mohsen' Choir data
    #test_dataset, test_iterator = ChoirDataIterator(CHOIR_TEST_DATA_FILE)
    NUM_CLASSES, NUM_ITEMS, NUM_TEST_EXAMPLES, test_dataset, test_iterator =  ChoirDataIterator(CHOIR_TEST_DATA_FILE)
    NUM_FEATURES = NUM_ITEMS - 2

    # Instantiate item memory
    # The file Item Memory exists
    initialized_im = read_hd_vectors(ITEM_MEMORY, -1);
    initialized_am = read_hd_vectors(ASSOC_MEMORY, -1);
    
    item_memory_t = tf.get_variable('item_memory_t', 
        initializer=tf.constant(initialized_im, dtype=tf.int32))
    
    associative_memory_t = tf.get_variable('associative_memory_t',
        initializer=tf.constant(initialized_am, dtype=tf.int32))
   
    channel_id_t = tf.get_variable('channel_id_t', dtype=tf.int32,
            initializer=np.array(range(NUM_FEATURES), dtype=np.int32))
    data_new_t, class_num_temp_t = test_iterator.get_next()
    data_temp_t = tf.cast(data_new_t, tf.int32)
    class_num_t = tf.cast(class_num_temp_t, tf.int32)

    data_t = NUM_ITEMS + data_temp_t
    
    data_vector_t = tf.gather(item_memory_t, data_t, axis=0,
            name="data_vector_t")
    

    channel_vector_t = item_memory_t[0:NUM_FEATURES,:]

    binded_vector_t = hlu_multiply(data_vector_t, channel_vector_t)
   
    z_t = tf.reduce_sum(binded_vector_t, 0)

    #**********************************************************************
    #                   Binary Associative Memory
    #**********************************************************************
    new_slice_t = 2 * tf.to_int32(tf.greater(z_t, tf.constant(0,
        dtype=tf.int32))) - 1
    zero = tf.constant(0, dtype=tf.int32)
    associative_memory_binary = 2 * tf.to_int32(tf.greater(associative_memory_t, zero))\
        - tf.to_int32(tf.not_equal(associative_memory_t, zero))
    final_binary = tf.matmul(associative_memory_binary, tf.reshape(new_slice_t, [-1, 1]))
    prediction_binary = tf.to_int32(tf.argmax(final_binary))
    
    ## get initializer ops
    test_glob_init = tf.global_variables_initializer()
    test_local_init = tf.local_variables_initializer()
    test_table_init = tf.tables_initializer()
    test_data_init = test_iterator.make_initializer(test_dataset)

sess_config=tf.ConfigProto(log_device_placement=True)
sess_config.gpu_options.allow_growth=True

# log device placement. helps in debugging
sess_test = tf.Session(graph=test_graph,
    config=sess_config)
#    config=tf.ConfigProto(log_device_placement=True))


#######################################################################

# Simple assertions to test that graphs are assigned as desired
assert item_memory_t.graph is test_graph
assert data_t.graph is test_graph
assert prediction_binary.graph is test_graph

#######################################################################
#                       SESSION RUN
#######################################################################

start_time = time.time()
#----------------------------------------------------------------------
#   Now initialize the Testing graph and check the trained 
#   associative memory and item memory have been copied
#----------------------------------------------------------------------
# Initialize the tables and all variables
sess_test.run(test_glob_init)
sess_test.run(test_local_init)
sess_test.run(test_table_init)
sess_test.run(test_data_init)

#print "Testing Graph Initialized."
#print "Restored Testing Item Memory."
#print "Restored Testing Associative Memory (trained earlier)."
#print(sess_test.run(associative_memory_binary))

# start the testing filename queue runner thread
coord_t = tf.train.Coordinator()
threads_t = tf.train.start_queue_runners(sess=sess_test, coord=coord_t)

## TESTING loop
count = 0
num_correct_bin = 0
num_correct_val = 0
num_correct_cos = 0
try:
    while not coord_t.should_stop():
        #prediction
        item_mem_out, associative_memory_binary_out, pred_bin, class_out_t =sess_test.run(
            [item_memory_t, associative_memory_binary, prediction_binary, class_num_t])
        count = count + 1

        printProgressBar(count, NUM_TEST_EXAMPLES, 
            prefix = "*** TEST "+DATASET+": "+str(count)+"/" +str(NUM_TEST_EXAMPLES),
            suffix = '', length = 50)
        
        # for vanilla binary
        if (pred_bin == class_out_t):
            num_correct_bin = num_correct_bin + 1

except Exception as e:
    #print ("MESSAGE: Exception received, " + str(e))
    coord_t.request_stop(e)
finally:
    coord_t.request_stop()

print ("\n##RESULT (BINARY): "+DATASET+ " has Correct " + str(num_correct_bin) + "/" +\
        str(count)+ " = " + "{:.2f}".format(((num_correct_bin * 100.0)/count)) + "%\n")

# Finish all the filename queue runner threads. Prevents zombies
coord_t.join(threads_t)

# close testing this session.
sess_test.close()

duration = time.time() - start_time

print ("Time Taken " + ("{0:.2f}").format(duration) + "s")
