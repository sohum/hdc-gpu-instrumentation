# hdc_modules.py - contains associative memory thresholder, assertion checks
# Sohum Datta
# November 2017, UC Berkeley
 

import tensorflow as tf

# Adds a new vector to the associative memory. Associative memory must have a
# class vectors stored as rows with number of columns == dimensions. 
# ** RETURNS A TENSORFLOW OPERATOR **
#
# Assertion checks are:
# * associative memory is of type tf.int32
# * new_vector is also of type int32
# * length of new vector and size of dimension are the same
# * the class_number is correct (< no. of rows)
def update_associative_memory_op(mem, new_vector, class_num):
    mem_val = tf.identity(mem)
    assertion_checks = tf.logical_and(
        tf.logical_and(
            tf.logical_and(mem_val.dtype == tf.int32, new_vector.dtype ==
                tf.int32),
            tf.equal(tf.shape(mem)[1], tf.shape(new_vector)[0])), 
            tf.logical_and(
                tf.greater_equal(class_num, 0),
                tf.logical_and(
                    tf.greater(tf.shape(mem)[1], class_num),
                    class_num.dtype == tf.int32)))
                
    assert_op = tf.Assert(assertion_checks, [mem_val, new_vector, class_num])
    
    with tf.control_dependencies([assert_op]):
        return mem[class_num, :].assign(new_vector)

# This function thresholds the associative memory. Bipolar code.
# ** RETURNS A TENSORFLOW OPERATOR **
def threshold_associative_memory_op(mem):
    zero = tf.constant(0, dtype=tf.int32)

    #TODO: Change this
    final_val =  2 * tf.to_int32(tf.greater(mem, zero)) - tf.to_int32(tf.not_equal(mem, zero))
    return tf.assign(mem, final_val)
