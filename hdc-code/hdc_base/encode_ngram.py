# encode_ngram.py - n-gram encoder from item memory addresses
# Sohum Datta
# November 2017, UC Berkeley

import tensorflow as tf
from hlu import *

# This is a helper functions, returning the assert condition checks for
# encoder_ngram algo. Notice that it has the exact same signature
def assert_encode_ngram(data_packet, n):
    ## This function must be separately called before calling the 
    ## (possibly heavy) encoding algorithm below. Can save a lot of computation
    ## Assertion Checks
    # 1. the data packet is an array of int32 of each row being a hyper-vector
    # arranged in increasing order of time. Number of rows > NGRAM size is a
    # must.
    # 3. HD vectors (no. of item_memory columns) has D > 1
    # 4. n-gram size is <= no. of data points in data_packet
    #
    # To use this, just type:
    # with tf.control_dependencies(assert_encode_ngrams(...)):
    #   ... encode_ngram(...)
    return tf.logical_and(data_packet.dtype == tf.int32, 
            tf.logical_and(data_packet.shape.ndims == 2, 
                tf.greater_equal(tf.shape(data_packet)[0], n)))


# Encodes the sum of n-grams over the entire data packet
# * data_packets: 2D array where each row is a HD vector (increasing time as
# later rows in the array)
# * n: the size of n-gram. n >= size of data_packet

def encode_ngram(data_packet, n):

    assertion_check = assert_encode_ngram(data_packet, n)
    assert_op = tf.Assert(assertion_check, [data_packet, n])
    
    # do assertion checks first
    with tf.control_dependencies([assert_op]):
        num_data = tf.shape(data_packet)[0]
        data_vector = tf.identity(data_packet)
         
        # this is a loop variable, holding the partially encoded n-gram
        # note that "_permuted" is misleading for the sentence below, 
        # as it is really the delayed version of data_vector
        # However, in the loop later this shall be the partially encoded value
        #data_vector_permuted = tf.concat([data_vector[1:,:], data_vector[:1,:]], 0,
        #    name = "data_vector_permuted")
        #data_vector_permuted = tf.concat([data_vector[-1:,:], data_vector[:-1,:]], 0,
        #    name = "data_vector_permuted")
        data_vector_permuted = hlu_delay(data_vector)

        # condition till which the n-gram loop shall be executed
        check_n = lambda n, data_vector_permuted: n > 0
        
        #######################################################################
        ## Protected section under ASSERTION assert_hlu
        # the body callback (HLU connection)
        # we only have to check this before the loop, as its loop invariant

        hlu_checks = tf.Assert(assert_hlu(data_vector, data_vector_permuted),
                [data_vector])
                
        
        with tf.control_dependencies([hlu_checks]):
            loop_body = lambda n, data_vector_permuted: (n-1, 
                delay_and_permuted_multiply(data_vector, data_vector_permuted))

        ## End of protected section under ASSERTION assert_hlu
        #######################################################################
   
        loop_n = n-1
        
        # this vector contains all the n-gram infos
        [final_n, vec] = tf.while_loop(check_n, loop_body, [loop_n, data_vector_permuted])

        # ORIGINAL
        # note that this is a delayed version, which needs to be rearranged 
        # the data n-gram corresponding to the first data-point is the last row.
        # vec_undelayed = tf.concat([vec[-1:,:], vec[:-2,:]], 0, name="vec_undelayed")
        
        # NOw
        # note that this is a delayed version, which needs to be rearranged 
        # the data n-gram corresponding to the first data-point is the first row.
        vec_undelayed = hlu_undelay(vec);
        #vec_undelayed = tf.concat([vec[1:,:], vec[:1,:]], 0, name="vec_undelayed")

        # ORIGINAL
        # Note that the last n-1 rows are redundant, must be trimmed
        #vec_trimmed = tf.slice(vec_undelayed, [0, 0], 
        #        [tf.shape(vec)[0] - (n-1),-1])

        # Note that the first n-1 rows are redundant, must be trimmed
        vec_trimmed = tf.slice(vec_undelayed, [n-1, 0], 
                [tf.shape(vec)[0] - (n-1),-1])

        # Add the partial products row-wise to get the n-gram sum
        return tf.reduce_sum(vec_trimmed, 0)
     
