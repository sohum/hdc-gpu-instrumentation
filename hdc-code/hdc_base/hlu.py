# hlu.py - HD Logic Unit operations in TensorFlow
# Sohum Datta
# November 2017, UC Berkeley
#
# inputs:
#   x - matrix where each row is a datapoint, 
#   y - matrix where each row is a datapoint, 
#   In both cases, times increases as we go down the rows (not the meaning of
#   "delay")

import tensorflow as tf
import numpy as np

def assert_hlu(x, y):
    ## Assertion checks for HLU operations
    # 1. x and y have the same shape
    # 2. both are 2D arrays
    # 3. x and y have same data type
    # To use this, just type:
    # with tf.control_dependencies(assert_hlu(...)):
    #   ... hlu modules...
    return tf.logical_and(x.shape.dims == 2,
            tf.logical_and(tf.equal(tf.shape(x), tf.shape(y)),
                x.dtype == y.dtype))

# NOTE: Permute is *LEFT SHIFT*
def permute(x):
    # Check that its atmost 2D array
    #if x.shape.ndims == 1:
    #    b = tf.concat([x[1:], x[:1]], 0)
    #elif x.shape.ndims == 2:
    #    b = tf.concat([x[:,1:], x[:,:1]], 1)
    b = tf.concat([x[:,1:], x[:,:1]], 1)
    return b

# Reverse of the delay implementation below
def hlu_undelay(x):
    b = tf.concat([x[1:,:], x[:1,:]], 0)
    #b = tf.concat([x[-1:,:], x[:-2,:]], 0)
    return b

def hlu_multiply(x, y):
    # NOTE: Don't confuse. In bipolar code, dot-product = _XNOR_ in binary 
    b = - tf.multiply(x, y)
    return b
#----------------------------------------------------------------------
#           Begin HLU Operations
#----------------------------------------------------------------------

# NOte the Delay implementation below
def hlu_delay(x):
    b = tf.concat([x[-1:,:], x[:-1,:]], 0)
    #b = tf.concat([x[-1:,:], x[:-2,:]], 0)
    return b

def delay_and_permute(x):
    a = permute(x)
    #a = tf.concat([x[:,1:], x[:,:1]], 1)    # LEFT SHIFT (Permute)
    #b = tf.concat([a[-1:,:], a[:-1,:]], 0)  # Delay
    b = hlu_delay(a)
    return b

def delay_and_multiply(x, y):
    a = hlu_multiply(x, y);
    #a = - tf.multiply(x, y);        # NOTE: - in front of Multiply XOR
    #b = tf.concat([a[-1:,:], a[:-1,:]], 0)  # Delay
    #b = tf.concat([a[1:,:], a[:1,:]], 0)
    b = hlu_delay(a)
    return b

def delay_and_permuted_multiply(x, y):
    y1 = permute(y)
    #y1 = tf.concat([y[:,1:],y[:,:1]], 1)    # LEFT SHIFT (Permute)
    a = hlu_multiply(x, y1)             
    #a = - tf.multiply(x, y1)        # NOTE: -1 in front of Multiply for XOR
    #b = tf.concat([a[-1:,:], a[:-1,:]], 0)  # Delay
    b = hlu_delay(a)
    #b = tf.concat([a[1:,:], a[:1,:]], 0)
    return b
