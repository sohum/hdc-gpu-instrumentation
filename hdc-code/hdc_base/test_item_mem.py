# test_item_mem.py - item memory testing program
# Sohum Datta
# November 2017, UC Berkeley


from item_mem import *
import numpy as np
import tensorflow as tf
import random as rnd

# Print full arrays without "..."
np.set_printoptions(threshold=np.inf)


CIM_HEAD_CONST = 127
HEAD_ADDR = 128

DIM = 8192

# Test strategy:
#   Assertions checks checked by hand, not done here (TODO)
#   * Select 20 possible values of ADDR (>=0, <= 127) so that CIM is not
#   triggered
#   * save the value of the initially created item_memory when reading these
#   addresses
#   * Now select 10 possible values of CIM HEAD  (>=0, <= 127)
#   * update the CIM_HEAD of the item memory
#   * After each update, check if address 128 is the same as designated
#   * Now select 10 random addresses in CIM for reading(>= 129)
#   * check the read value with that calculated by flipping bits separately
#   from vector stored at CIM_HEAD

D = tf.constant(DIM, dtype=tf.int32)
CIM_HEAD = tf.constant(CIM_HEAD_CONST, dtype=tf.int32, name='CIM_HEAD')
read_addr = tf.placeholder(tf.int32, shape=[])
new_CIM_HEAD = tf.placeholder(tf.int32, shape=[])

#assertion_item_mem = tf.Assert(assert_create_item_memory(CIM_HEAD, D),
#        [CIM_HEAD, D])
#
#with tf.control_dependencies([assertion_item_mem]):
item_mem = tf.Variable(create_item_memory(CIM_HEAD, D), name="item_mem")
expanded_item_mem = expand_item_memory(item_mem)


#assertion_read_value = tf.Assert(assert_read_item_memory(item_mem, read_addr),
#        [item_mem, read_addr])
#
#with tf.control_dependencies([assertion_read_value]):
read_value = read_item_memory(item_mem, read_addr)

update_cim_head_op = update_item_memory_cim_head(new_CIM_HEAD, item_mem)

init_op = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init_op)
   
    print "Testing item_mem.py...\n"
    print "Current CIM_HEAD is "+str(CIM_HEAD_CONST)+", DIM = "+str(DIM)
    print(sess.run(read_value, feed_dict={read_addr: CIM_HEAD_CONST}))

    # Now initialize ten values for reading from addresses 0 to 127, and read
    # those addresses into an array
    READ_ADDR_LIST = rnd.sample(range(128), 10)
    saved_values = np.zeros((10, DIM))
    for i in range(10):
        saved_values[i,:] = sess.run(read_value, feed_dict={read_addr:
            READ_ADDR_LIST[i]})

    for i in range(10):
        # Chose a random CIM_HEAD and update item_memory
        print "\n"
        CIM_HEAD_CONST = rnd.randint(0, 127)
        
        sess.run(update_cim_head_op, feed_dict={new_CIM_HEAD: CIM_HEAD_CONST})
        print "Updated CIM_HEAD to " + str(CIM_HEAD_CONST)
        current_cim_vector = sess.run(read_value, feed_dict={read_addr:
            HEAD_ADDR})
        
        required_cim_vector = sess.run(read_value, feed_dict={read_addr: CIM_HEAD_CONST})
        if np.array_equal(required_cim_vector, current_cim_vector):
            print "CIM Set Successfully \t\t[Passed]"
        else:
            print "CIM set unsuccessful \t\t[FAILED]\nrequired_cim_vector="
            print(required_cim_vector) 
            print "current_cim_vector="
            print(current_cim_vector) 
       
        ## Read the expanded Item memory at this point
        expanded_item_mem_array = sess.run(expanded_item_mem)
        
        # read the non-CIM values
        current_read_values = np.zeros((10, DIM))
        current_expanded_read_values = np.zeros((10, DIM))
        for k in range(10):
            current_read_values[k,:] = sess.run(read_value, feed_dict=
                {read_addr:READ_ADDR_LIST[k]})
            current_expanded_read_values [k,:] = expanded_item_mem_array[READ_ADDR_LIST[k],:]
        
        # check with saved values earlier
        # Test both the value read from read_item_memory and from expanded
        # version of item memory
        if np.array_equal(current_read_values, saved_values) and\
        np.array_equal(current_expanded_read_values, saved_values):
            print "non-CIM Read test done.\t\t[Passed]"
        else:
            print "non-CIM Read test done.\t\t[FAILED]"
            print "non-CIM addresses probed ="
            print (READ_ADDR_LIST)
            print "saved_values="
            print(saved_values)
            print "current_read_values="
            print (current_read_values)

        # now check if CIM values are outputtted successfully
        CIM_ADDR_LIST = rnd.sample(range(129, 256), 10)

        for k in range(10):
            num_flips = (DIM // 256) * (CIM_ADDR_LIST[k] - 128)

            flip_vector = np.ones(DIM, dtype=int)
            flip_vector[0:num_flips] = -1
            required_cim = np.round(np.multiply(current_cim_vector,
                flip_vector))

            current_cim = np.round(sess.run(read_value,
                feed_dict={read_addr:CIM_ADDR_LIST[k]}))
            # read value from the expanded memory as well
            current_expanded_continous_val = expanded_item_mem_array[CIM_ADDR_LIST[k],:]
            
            # Test both the value read from read_item_memory and from expanded
            # version of item memory
            if (current_cim == required_cim).all() and\
            (current_expanded_continous_val == required_cim).all():
                print "CIM Read test done. \t\t[Passed]"
            else:
                print "CIM Read test done. \t\t[FAILED]"
                print "CIM address probed = "+str(CIM_ADDR_LIST[k])
                print "(current_cim==required_sim)"
                print (current_cim==required_cim)

