# item_mem.py - item memory initializer
# Sohum Datta
# November 2017, UC Berkeley
 

import tensorflow as tf
import numpy as np
import random as rnd
import math as mth
import os

# The Item Memory generated for hardware must be same across applications.
# The wollowing file saves the item memory in the following format:
# Filename = <ITEM_MEMORY_FILENAME> + <NUM_REPS>.dat
# NUM_REPS is defined in create_item_memory_np below
# ITEM_MEMORY_FILENAME = "item_memory"


# This functions intitializes the constant item_memory. Bipolar Code.
#
# There are 2^10 = 1024 possible characters in Item Memory.
# The lower half of the character set (chars 1 - 1024) are constants.
# The upper half of the character set (chars 1025 onwards) are contiuous HD
# vectors satisfying triangle inequality (for Hamming distance)
# 
# The variable CIM_HEAD is the origin of continuous item memory. Its assigned
# an address 1 - 1024

# Assertion checks for create_item_memory:
# * D is tf.int32 with value > 256 and divisible by 256
# * CIM_HEAD is tf.int32 with value between 0 and 127 (included)

# Numpy version. Speeds up the computation if you don't have to reset the CIM
# head in the middle (mostly true). Bipolar Code.
# Also the number of (non-CIM) items can be changed 
# Checked manually -- done!



def create_item_memory_np(CIM_HEAD, D, NUM_ITEMS=1023):
    
    #if number of items required is larger than 1024, 
    # will randomly permute the basic 1024 vectors.
    # number of permutations needed (atleast once) is 
    #in the variable "num_reps".
    num_reps = (NUM_ITEMS/1024)+1
   
    #final_filename = ITEM_MEMORY_FILENAME + str(num_reps) + ".dat";
    
    #if(os.path.isfile(final_filename)):
    #    # The file exists
    #    file = open(final_filename, 'r');
    #    mem = np.fromfile(final_filename, dtype=np.int32);
    #   print "create_item_memory_np: Loaded Item Memory data "+ final_filename
    #    file.close();
    #else:
   # Make the item memory and save in file
    
    # Create the basic item memory first
    base = np.ones(D)
    pre_mem = list()
    for i in range(int(D/2)):
        base[i] = -1
    
    # create the basic 1024 items
    for j in range(1024):
        pre_mem.append(np.random.permutation(base))
    
    #These vectors below will be there in hardware ROM
    mem_hw = np.array(pre_mem)
    mem = mem_hw
    # The number of 1s and -1 are exactly equal 
    
    while (num_reps > 1):
        temp = np.transpose(mem_hw)
        #the random.permutation function below can only permute rows
        #we need column permutation. Hence transpose.
        new_mem = np.transpose(np.random.permutation(temp))
        mem = np.concatenate((mem, new_mem), axis=0)
        num_reps = num_reps - 1
    
    mem.astype(np.int32)
    
    return mem
    

def create_lsh_memory_np(D, NUM_LSH):
    pre_mem = list()
    for j in range(NUM_LSH):
        mem = np.random.normal(0, 1.0, D)
        mem = (1000*mem)/np.linalg.norm(mem)
        mem.astype(np.int32)
        pre_mem.append(mem)
    return np.array(pre_mem)

def assert_create_item_memory(CIM_HEAD, D):
    return tf.logical_and(
        tf.logical_and(
            tf.logical_and(CIM_HEAD.dtype == tf.int32, 
                CIM_HEAD.shape.ndims == 0),
            tf.logical_and(
                tf.greater_equal(CIM_HEAD, 0), 
                tf.less_equal(CIM_HEAD, 127))),
        tf.logical_and(
            tf.logical_and(D.dtype == tf.int32,
                D.shape.ndims == 0),
            tf.logical_and(
                tf.greater_equal(D, 256),
                tf.equal(tf.mod(D, 256), 0))))
                

def create_item_memory(CIM_HEAD, D):
    assert_op = tf.Assert(assert_create_item_memory(CIM_HEAD, D), 
            [CIM_HEAD, D])
    
    with tf.control_dependencies([assert_op]):
        # The CIM_HEAD is *always* the 129th row in this array
        init_mem = tf.random_uniform([128, D])
        init_mem = 2 * tf.to_int32(tf.greater(init_mem, 0.5)) - 1
        cim_vector = tf.slice(init_mem, [CIM_HEAD,0], [1, D])   #set the CIM_HEAD
        return tf.to_int32(tf.concat([init_mem, cim_vector], 0))


# Assertion checks for reading item memory
# * addr is tf.int32 between 0 and 255 (included)
# * "mem" is of type tf.int32 with 129 rows

def assert_read_item_memory(mem, addr):
    # This is very important, as item memory will usually be a 
    # tensorflow variable. The best practise is to remove datatype reference
    # (implicit in tf.Variable) and convert it to runtime dtypes:
    # Eg. "tf.int32_ref" to "tf.inbt32"
    mem_val = tf.identity(mem) 

    D = tf.shape(mem_val)[1]
    rows = tf.shape(mem_val)[0]
    return tf.logical_and(
        tf.logical_and(
            tf.logical_and(addr.dtype == tf.int32, 
                addr.shape.ndims == 0),
            tf.logical_and(
                tf.greater_equal(addr, 0), 
                tf.less_equal(addr, 255))),
            tf.logical_and(mem_val.dtype == tf.int32,
                tf.equal(rows, 129)))


# Return the vector based on the tf.Variable "mem" supplied of type tf.int32
# Here each row is a hypervector. Assertion checks required before this.

def read_item_memory(mem, addr):
    assert_op = tf.Assert(assert_read_item_memory(mem, addr), 
            [mem, addr])
    
    with tf.control_dependencies([assert_op]):
        read_addr = tf.to_int32(tf.mod(addr, 128))
        read_value = mem[read_addr, :]

        D = tf.to_int32(mem.shape[1])
        K = D/256  #ASSERT D is divisible by 256
        
        flip_vect_slice1 = - tf.ones([1, read_addr * K])
        flip_vect_slice2 = tf.ones([1, D - read_addr * K])
        cim_flip_vector = tf.cond(read_addr > 0, 
            lambda: tf.to_int32(tf.concat([flip_vect_slice1, flip_vect_slice2], 1)),
            lambda: tf.ones([1, D], dtype=tf.int32))

        # need to flatten this to 1D vector
        multiplied_vector = tf.multiply(mem[128,:], cim_flip_vector),
        
        # If statement returning the actual read vector
        return tf.cond(addr < 128, 
            lambda: read_value, 
            lambda: tf.reshape(multiplied_vector, [-1]),
            strict=True)

# Assertion checks for update_item_memory_cim_head (below)
# * item memory must be a tf.Variable of type int32
# * must have 129 rows, the number of columns are not checked
# * CIM_HEAD must be a scalar of type int32
# * must be >=0 and <= 127

def assert_update_item_memory_cim_head(CIM_HEAD, mem):
    mem_val = tf.identity(mem) 

    D = tf.shape(mem_val)[1]
    rows = tf.shape(mem_val)[0]

    return tf.logical_and(
        tf.logical_and(
            tf.logical_and(CIM_HEAD.dtype == tf.int32, 
                CIM_HEAD.shape.ndims == 0),
            tf.logical_and(
                tf.greater_equal(CIM_HEAD, 0), 
                tf.less_equal(CIM_HEAD, 127))),
            tf.logical_and(mem_val.dtype == tf.int32,
                tf.equal(rows, 129)))

        
# Update the CIM_HEAD of an Item Memory, *** RETURNS A TENSORFLOW OPERATION ***
# Assumed to be tf.Variable which is initialized by create_item_memory (int32)
# Assertion checks must be done prior to this. 

def update_item_memory_cim_head(CIM_HEAD, mem):
    assert_op = tf.Assert(assert_update_item_memory_cim_head(CIM_HEAD, mem), 
            [CIM_HEAD, mem])
    
    with tf.control_dependencies([assert_op]):
        # first read the value that replaces thje 129th row (CIM HEAD)
        # Assertion checks should confirm that this is in address 0 - 127!
        
        new_vector_assertions = tf.Assert(assert_read_item_memory(mem, CIM_HEAD),
                [mem, CIM_HEAD])
        with tf.control_dependencies([new_vector_assertions]):
            new_vector = read_item_memory(mem, CIM_HEAD)

        update_cim_head_op = tf.assign(mem[128,:], new_vector,
            name='update_cim_head_op')
        
        return update_cim_head_op


# Return an expanded version of Item Memory
# THIS array has 256 rows. The convention remains: first 128 are normal items
# and the next 128 rows are continous items with CIM_HEAD is 129. This is
# useful for gather/scatter operations (encoding n-grams for example). 
# Assertion checks are:
# * item memory must be a tf.Variable of type int32
# * must have 129 rows, the number of columns are not checked
#

def expand_item_memory(mem):
    cim_head_addr = tf.constant(128, dtype=tf.int32)
    CIM_HEAD = read_item_memory(mem, cim_head_addr)

    mem_val = tf.identity(mem) 
    D = tf.shape(mem_val)[1]
    rows = tf.shape(mem_val)[0]
    
    dummy = tf.zeros([127, D], tf.int32)
       
    addr = tf.constant(129, dtype=tf.int32)

    # assertion check for the entire function
    assertion_check = tf.logical_and(mem_val.dtype == tf.int32,
        tf.equal(rows, 129))

    # Concatenate zeros to the unfilled CIM spaces of the expanded array
    assert_op = tf.Assert(assertion_check, [rows, mem_val])
   
    expanded_mem = tf.to_int32(mem_val)
    
    # Check assertions for the entire function and continue
    with tf.control_dependencies([assert_op]):
        check_addr = lambda mem, expanded_mem, addr: addr < 256 
        
        def loop_body(mem, expanded_mem, addr):
            inserted_row = tf.reshape(read_item_memory(mem, addr), [1, -1])
            new_array = tf.concat([expanded_mem, inserted_row], 0)
            return mem, new_array, addr+1
        
        [final_mem, final_expanded_mem, addr] = tf.while_loop(check_addr, loop_body,
            [mem_val, expanded_mem, addr], shape_invariants=[mem_val.get_shape(),
                tf.TensorShape(None), addr.get_shape()])
        
        return final_expanded_mem
 
