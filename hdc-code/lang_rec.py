#!/usr/bin/env python
import tensorflow as tf
import time
import os
import glob
import math
import subprocess
import sys

import argparse

from hdc_base.hlu import *
from hdc_base.assoc_mem import *
from hdc_base.encode_ngram import *

from input_pipes.input_textline import *

from utils.make_dir import *
from utils.printProgressBar import *
from utils.next_power_of_two import *
from utils.print_dataset_stats import *
from utils.read_hd_vectors import *
from utils.write_hd_vectors import *
from utils.create_item_memory_np import *

## Argument setup
parser = argparse.ArgumentParser(description='HD Demo 1 simulator for Language Recognition')
parser.add_argument("itemmemory", help="Binary Item Memory for FlexEMG")
parser.add_argument("assocmem", help="Binary AM file for FLexEMG")

args = parser.parse_args()

#Only ERROR messages by Tensorflow
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}

##################################################################################
#               * MACHINE SIMULATION SETUP *
##################################################################################
### Please make changes below only!                                             

# address to use for heade of continous item memory. Can be 0 - 127 (DEFAULT)
CIM_HEAD = 127

# Size of ngram
NGRAM_SIZE = 4

##################################################################################
#               * LANGUAGE RECOGNITION SIMULATION SETUP *
##################################################################################

TEST_DIR = "./dataset/europarl_lang/testing_texts/tf_10/"

# Number of classes and features in associative memory
NUM_CLASSES = 23
NUM_FEATURES = NGRAM_SIZE

# List of ASCII printable characters except special characters (total 54)
accepted_char_list = list(
        "abcdefghijklmnopqrstuvwxyz -*+=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")

##################################################################################
#               * DELIVERABLES OUTPUT DIRECTORY SETUP *
##################################################################################

# HDC Dimension used
DIM = 2048
DATASET = "LANG_EUROPARL"


ITEM_MEMORY = args.itemmemory
ASSOC_MEMORY = args.assocmem

#-----------------------------   END OF SETUP ------------------------------------

##################################################################################
##################################################################################

_test_cmd = "wc -l " + TEST_DIR + "* | grep \"total\" | cut -d ' ' -f 3"
NUM_TEST_EXAMPLES = int(subprocess.check_output(_test_cmd, shell=True).rstrip())


# Some variables to set the strings for classes
get_basename = lambda i: os.path.basename(i)
remove_ext = lambda j: j.split(".")[0]
# This removes the directory path and just gets <label>.txt

dummy2 = ['ron', 'swe', 'ita', 'slv', 'nld',
        'nob', 'por', 'lit', 'hun', 'slk',
        'dan', 'fin', 'pol', 'est', 'ces',
        'deu', 'afr', 'ell', 'lav', 'spa',
        'bul', 'eng', 'fra']

#----------------------------------------------------------------------
#                   Testing Module
#----------------------------------------------------------------------

test_graph = tf.Graph()
with test_graph.as_default():
    # Copy Common Variables from the training graph
    n_t = tf.constant(NGRAM_SIZE)

    # Instantiate item memory
    initialized_im = read_hd_vectors(ITEM_MEMORY, -1);
    initialized_am = read_hd_vectors(ASSOC_MEMORY, -1);
    
    item_memory_t = tf.get_variable('item_memory_t', 
        initializer=tf.constant(initialized_im, dtype=tf.int32))
    
    associative_memory_t = tf.get_variable('associative_memory_t', dtype=tf.int32,
        initializer=tf.constant(initialized_am, dtype=tf.int32))
   
    # Make a Table. Their Indices will map the Character to HD vectors. Have one
    # spare mapping for a character none of the above.
    accepted_chars = tf.constant(accepted_char_list)
    char_table = tf.contrib.lookup.index_table_from_tensor(mapping=accepted_chars,
        num_oov_buckets=1)
    
    accepted_classes = tf.constant(dummy2)
    # This maps label strings to class numbers. Have one spare class numbers for
    # unknown label strings.
    class_table = tf.contrib.lookup.index_table_from_tensor(mapping=accepted_classes,
        num_oov_buckets=1)
    
    files_t = tf.train.match_filenames_once(TEST_DIR + "*.txt", name="files_t")
    
    label_t, line_t = input_textline_pipe(files_t)
    
    class_num_t = tf.convert_to_tensor(tf.to_int32(class_table.lookup(label_t)),
            dtype=tf.int32)
     
    char_line_t = tf.string_split([line_t], delimiter="").values 
    
    int_line_t = tf.convert_to_tensor(tf.to_int32(
        char_table.lookup(char_line_t)), dtype=tf.int32)
   
    #TODO: Thresholded the encoded ngram vector
    data_packet_t = tf.gather(item_memory_t, int_line_t, axis=0,
            name="data_packet_t")
    z_t = encode_ngram(data_packet_t, n_t)

    #**********************************************************************
    #                   Binary Associative Memory
    #**********************************************************************
    new_slice_t = 2 * tf.to_int32(tf.greater(z_t, tf.constant(0,
        dtype=tf.int32))) - 1
    zero = tf.constant(0, dtype=tf.int32)
    associative_memory_binary = 2 * tf.to_int32(tf.greater(associative_memory_t, zero))\
        - tf.to_int32(tf.not_equal(associative_memory_t, zero))
    
    # Multiply in Bipolar code is XNOR
    # this matmult will count number of 1's - 0's in XNOR of two vectors
    #   = 2 * (Number of 1's in XNOR) - D
    #   = 2 * (D - hamming_dist) - D
    #   = D - 2 * hamming_dist
    # So the final prediction will have smalles hamming distance = largest dot
    # product

    final_binary = tf.matmul(associative_memory_binary, tf.reshape(new_slice_t, [-1, 1]))
    prediction_binary = tf.to_int32(tf.argmax(final_binary))
    
    ## get initializer ops
    test_glob_init = tf.global_variables_initializer()
    test_local_init = tf.local_variables_initializer()
    test_table_init = tf.tables_initializer()

# log device placement. helps in debugging
sess_config=tf.ConfigProto(log_device_placement=True)
sess_config.gpu_options.allow_growth=True

sess_test = tf.Session(graph=test_graph, 
    config=sess_config)


#######################################################################

# Simple assertions to test that graphs are assigned as desired
assert sess_test.graph is test_graph
assert item_memory_t.graph is test_graph
assert prediction_binary.graph is test_graph

#######################################################################
#                       SESSION RUN
#######################################################################

start_time = time.time()
# Initialize the tables and all variables
sess_test.run(test_glob_init)
sess_test.run(test_local_init)
sess_test.run(test_table_init)

print "Testing Graph Initialized."
print "Restored Testing Item Memory."
#print(sess_test.run(item_memory_t))
print "Restored Testing Associative Memory (trained earlier)."
#print(sess_test.run(associative_memory_t))

#print(dummy2)

# start the testing filename queue runner thread
coord_t = tf.train.Coordinator()
threads_t = tf.train.start_queue_runners(sess=sess_test, coord=coord_t)

## TESTING loop
count = 0
num_correct_bin = 0
num_correct_cos = 0
num_correct_val = 0
try:
    while not coord_t.should_stop():
        # get the next example
        item_mem_out, associative_memory_binary_out, pred_bin, class_out_t =sess_test.run(
            [item_memory_t, associative_memory_binary, prediction_binary, class_num_t])
        count = count + 1

        printProgressBar(count, NUM_TEST_EXAMPLES, 
            prefix = "*** TEST "+str(count)+"/" +str(NUM_TEST_EXAMPLES),
            suffix = '', length = 50)
        

        # for vanilla binary
        if (pred_bin == class_out_t):
            num_correct_bin = num_correct_bin + 1
except Exception as e:
#    print ("Exception received: " + str(e))
    coord_t.request_stop(e)
finally:
    coord_t.request_stop()

print ("\n##RESULT (BINARY): "+DATASET+ " has Correct " + str(num_correct_bin) + "/" +\
        str(count)+ " = " + "{:.2f}".format(((num_correct_bin * 100.0)/count)) + "%\n")


# Finish all the filename queue runner threads. Prevents zombies
coord_t.join(threads_t)

# close testing this session.
sess_test.close()

duration = time.time() - start_time
print ("Time Taken " + ("{0:.2f}").format(duration) + "s")
