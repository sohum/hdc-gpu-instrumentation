sono pienamente daccordo con quanto ha detto lonorevole miller sullimportanza di questa misura come pacchetto di intervento straordinario e come tale esso deve essere considerato 
se non riusciamo a compiere passi avanti verso una soluzione della questione di gaza anche il processo di pace sara pieno di difficolta 
nel libro verde nuove prospettive per lo spazio europeo della ricerca la commissione offre unanalisi propone soluzioni e sottolinea quale elemento cruciale sia la creazione di una societa basata sulla conoscenza 
per iscritto   la relazione si basa sulla comunicazione della commissione europea dal titolo migliorare la qualita della formazione degli insegnanti  e sottolinea la necessita che gli stati membri riformino i propri sistemi di istruzione nazionali al fine di migliorare la formazione degli insegnanti 
 quanto ho capito eravamo daccordo sul fatto che se volevamo che si potessero effettuare le ristrutturazioni dovevamo fare in modo che i dipendenti fossero coinvolti nel processo 
la commissione incentiva pertanto la creazione di veri e propri ghetti per i poveri a scapito della diversita sociale 
invito quindi il consiglio a cambiare opinione a questo riguardo e a garantire per il programma una durata di tre anni completi anche perche lo sportello euro per lavviamento e il meccanismo di garanzia  sono crediti rotativi che si esauriscono completamente soltanto dopo  anni 
adesso finalmente approviamo una legislazione contro le discriminazioni a livello di unione europea 
 tale scopo verra istituito un pannello di controllo o scoreboard da rendere accessibile a tutti 
io sono un liberale ma sono comunque capace di riconoscere le strutture e anche in parlamento esistono strutture 
