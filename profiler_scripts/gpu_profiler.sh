#!/bin/bash
# gpu_profiler: prepares the table for gpu profiling

## Section 1. Find the summary of GPU Kernels:
# * total time spent in copying from Host (CPU) to Device (GPU)
# * number of calls of memcpy from Host to Device
# * total time spent in copying from Device (GPU) to Host (CPU)
# * number of calls of memcpy from Device to Host

nvprof --profile-api-trace none -- $COMMAND 2> summary.log


## Section 2. Find the total memories copied into and out of GPU
nvprof --print-gpu-trace $COMMAND 2> gputrace.log

# Total number of bytes copied from Host (CPU) to Device (GPU)
num_bytes_htod=$(grep "CUDA memcpy HtoD" gputrace.log | tr -s ' ' | tr ' ' ',' | cut -d, -f8 | numfmt --from si --suffix B | paste -sd+ - | bc | numfmt --to si --suffix B)

# Total number of bytes from Device (GPU) to Host (CPU)
num_bytes_dtoh=$(grep "CUDA memcpy DtoH" gputrace.log | tr -s ' ' | tr ' ' ',' | cut -d, -f8 | numfmt --from si --suffix B | paste -sd+ - | bc | numfmt --to si --suffix B)


# Section 3. 
